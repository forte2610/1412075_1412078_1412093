var express = require('express');
var router = express.Router();

var Product = require('../model/product');
var User = require('../model/user');
var Order = require('../model/order');
var Cart = require('../model/cart');

var StatUtilities = require('../utilities/stat');

router.get('/', isLoggedIn, isAdmin, function (req, res) {
    res.redirect('/admin/dashboard');
});

router.get('/dashboard', isLoggedIn, isAdmin, function (req, res) {
    Order.find(function (err, orders) {
        if (err) return res.sendStatus(500);
        var top10 = StatUtilities.getTop10(orders);
        res.render('admin/dashboard', { top10Data: top10 });
    });
});

router.get('/catalogue', isLoggedIn, isAdmin, function (req, res) {
    Product.find(function (err, products) {
        if (err) return res.sendStatus(500);
        res.render('admin/catalogue', {productList: products});
    });
});

router.get('/catalogue/add-product', isLoggedIn, isAdmin, function (req, res) {
    res.render('admin/catalogue_add');
});

router.post('/catalogue/add-product', isLoggedIn, isAdmin, function (req, res) {
    req.checkBody("thumbnail", "Preview image must be a URL!").notEmpty().isURL();
    req.checkBody("title", "Title must not be empty!").notEmpty();
    req.checkBody("description", "Description must not be empty!").notEmpty();
    req.checkBody("price", "Price must be a number!").notEmpty().isNumeric();
    req.checkBody("genres", "Genres list must not be empty!").notEmpty();
    req.checkBody("platform", "Platform must not be empty!").notEmpty();
    req.checkBody("quantity", "Copies in stock must be a number!").notEmpty().isNumeric();
    var error = req.validationErrors();
    if (error) {
        res.render('admin/catalogue_add', {hasErrors: error})
    } else {
        var productData = {
            imagePath: req.body.thumbnail,
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            genre: req.body.genres.split(","),
            platform: req.body.platform,
            quantity: req.body.quantity
        };
        var newProduct = new Product(productData);
        newProduct.save(function (error, data) {
            if (error) {
                res.json(error);
            }
            res.redirect('/catalogue');
        });
    }
});

router.get('/catalogue/update/:id', isLoggedIn, isAdmin, function (req, res) {
    var productId = req.params.id;
    Product.findOne({_id: productId}, function (err, product) {
        if (!product) {
            return res.write('No such product!')
        }
        else {
            res.render('admin/catalogue_modify', {product: product})
        }
    });
});

router.post('/catalogue/update/:id', isLoggedIn, isAdmin, function (req, res) {
    req.checkBody("thumbnail", "Preview image must be a URL!").notEmpty().isURL();
    req.checkBody("title", "Title must not be empty!").notEmpty();
    req.checkBody("description", "Description must not be empty!").notEmpty();
    req.checkBody("price", "Price must be a number!").notEmpty().isNumeric();
    req.checkBody("genres", "Genres list must not be empty!").notEmpty();
    req.checkBody("platform", "Platform must not be empty!").notEmpty();
    req.checkBody("quantity", "Copies in stock must be a number!").notEmpty().isNumeric();
    var error = req.validationErrors();
    if (error) {
        res.render('admin/catalogue_modify', {hasErrors: error})
    } else {
        var productId = req.params.id;
        Product.findOne({_id: productId}, function (err, product) {
            if (!product) {
                return res.write('No such product!')
            }
            else {
                product.imagePath = req.body.thumbnail;
                product.title = req.body.title;
                product.description = req.body.description;
                product.price = req.body.price;
                product.genre = req.body.genres.split(",");
                product.platform = req.body.platform;
                product.quantity = req.body.quantity;
                product.save(function (error, data) {
                    if (error) {
                        res.write(error);
                    }
                    res.redirect('/catalogue');
                });
            }
        });
    }
});

router.get('/catalogue/delete/:id', isLoggedIn, isAdmin, function (req, res) {
    var productId = req.params.id;
    Product.findOneAndRemove({_id: productId}, function (err, product) {
        if (err) {
            return res.write('Error accessing database!');
        }
        res.redirect('/catalogue');
    });
});

router.get('/users/user-list', isLoggedIn, isAdmin, function (req, res) {
    User.find(function (err, users) {
        if (err) return res.sendStatus(500);
        res.render('admin/users', {userList: users});
    });
});

router.get('/users/add-user', isLoggedIn, isAdmin, function (req, res) {
    res.render('admin/user_add');
});

router.post('/users/add-user', isLoggedIn, isAdmin, function (req, res) {
    req.checkBody("email", "You entered an invalid e-mail address!").notEmpty().isEmail();
    req.checkBody("role", "Role must not be empty!").notEmpty();
    req.checkBody("thumbnail", "Profile picture must be a URL!").isURL();
    req.checkBody("firstname", "First name must not be empty!").notEmpty();
    req.checkBody("lastname", "Last name must not be empty!").notEmpty();
    req.checkBody("address", "Address must not be empty!").notEmpty();
    req.checkBody("phone", "A phone number is required!").notEmpty().isNumeric();
    req.checkBody("dob", "You entered an invalid date!").notEmpty().isDate();
    var hasErrors = req.validationErrors();
    if (hasErrors) {
        res.render('admin/user_add', {hasErrors: hasErrors})
    } else {
        var userData = {
            email: req.body.email,
            role: req.body.role,
            imagePath: req.body.thumbnail,
            firstName: req.body.firstname,
            lastName: req.body.lastname,
            address: req.body.address,
            phone: req.body.phone,
            dob: req.body.dob
        };
        var newUser = new User(userData);
        newUser.save(function (error, data) {
            if (error) {
                res.write(error);
            }
            res.redirect('/users/user-list');
        });
    }
});

router.get('/users/update/:id', isLoggedIn, isAdmin, function (req, res) {
    var userId = req.params.id;
    User.findOne({_id: userId}, function (err, user) {
        if (!user) {
            return res.write('No such user!')
        }
        else {
            res.render('admin/user_modify', {user: user})
        }
    });
});

router.post('/users/update/:id', isLoggedIn, isAdmin, function (req, res) {
    req.checkBody("email", "You entered an invalid e-mail address!").notEmpty().isEmail();
    req.checkBody("role", "Role must not be empty!").notEmpty();
    req.checkBody("thumbnail", "Profile picture must be a URL!").isURL();
    req.checkBody("firstname", "First name must not be empty!").notEmpty();
    req.checkBody("lastname", "Last name must not be empty!").notEmpty();
    req.checkBody("address", "Address must not be empty!").notEmpty();
    req.checkBody("phone", "A phone number is required!").notEmpty().isNumeric();
    req.checkBody("dob", "You entered an invalid date!").notEmpty().isDate();
    var hasErrors = req.validationErrors();
    if (hasErrors) {
        res.render('admin/user_modify', {hasErrors: hasErrors})
    } else {
        var userId = req.params.id;
        User.findOne({_id: userId}, function (err, user) {
            if (!user) {
                return res.write('No such user!')
            }
            else {
                user.email = req.body.email;
                user.role = req.body.role;
                user.imagePath = req.body.thumbnail;
                user.firstName = req.body.firstname;
                user.lastName = req.body.lastname;
                user.address = req.body.address;
                user.phone = req.body.phone;
                user.dob = req.body.dob;
                user.save(function (error, data) {
                    if (error) {
                        res.write(error);
                    }
                    res.redirect('/users/user-list');
                });
            }
        });
    }
});

router.get('/users/delete/:id', isLoggedIn, isAdmin, function (req, res) {
    var userId = req.params.id;
    User.findOneAndRemove({_id: userId}, function (err, user) {
        if (err) {
            return res.write('Error accessing database!');
        }
        res.redirect('/users/user-list');
    });
});

router.get('/orders', isLoggedIn, isAdmin, function (req, res) {
    Order.find(function (err, orders) {
        if (err) return res.sendStatus(500);
        var cart;
        orders.forEach(function (order) {
            cart = new Cart(order.cart);
            order.items = cart.generateArray();
        });
        res.render('admin/orders', {orderList: orders});
    }).populate('user');
});

router.get('/orders/flip/:id', isLoggedIn, isAdmin, function (req, res) {
    var orderId = req.params.id;
    Order.findOne({_id: orderId}, function (err, order) {
        if (!order) {
            return res.write('No such order!')
        }
        else {
            if (order.received) order.received = false;
            else order.received = true;
            order.save(function (error, data) {
                if (error) {
                    console.log(error);
                }
                res.redirect('/admin/orders');
            });
        }
    });
});

function salesChart(chartLabels, chartData) {
    var data = {
        labels: chartLabels,
        datasets: [
            {
                label: "",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: chartData
            }]
    };
    var ctx = document.getElementById("salesChart").getContext("2d");
    new Chart(ctx).Line(data);
}

router.post('/stat/', isLoggedIn, isAdmin, function (req, res) {
    var top10;
    Order.find(function (err, orders) {
        if (err) return res.sendStatus(500);
        top10 = StatUtilities.getTop10(orders);
    });
    var labels = [];
    var data = [];
    var scale = req.body.scale;
    if (scale === 'Week') {
        Order.aggregate([
            {
                $group: {
                    _id: {$week: '$date'},
                    count: {$sum: '$cart.totalQty'}
                }
            }]).exec(function (e, d) {
            for (i = 0, len = d.length; i < len; ++i) {
                labels.push(d[i]._id);
                data.push(d[i].count);
            }
            res.render('admin/dashboard', {
                scale: scale,
                statData: d,
                top10Data: top10
            });
        });
    } else if (scale === 'Month') {
        Order.aggregate([
            {
                $group: {
                    _id: {$month: '$date'},
                    count: {$sum: '$cart.totalQty'}
                }
            }]).exec(function (e, d) {
            res.render('admin/dashboard', {
                scale: scale,
                statData: d,
                top10Data: top10
            });
        });
    } else if (scale === 'Quarter') {
        Order.aggregate([
            {
                $project: {
                    "quarter": {
                        $cond: [{$lte: [{$month: "$date"}, 3]},
                            "First",
                            {
                                $cond: [{$lte: [{$month: "$date"}, 6]},
                                    "Second",
                                    {
                                        $cond: [{$lte: [{$month: "$date"}, 9]},
                                            "Third",
                                            "Fourth"]
                                    }]
                            }]
                    }
                },
                $group: {
                    _id: '$quarter',
                    count: {$sum: '$cart.totalQty'}
                }
            }]).exec(function (e, d) {
            res.render('admin/dashboard', {
                scale: scale,
                statData: d,
                top10Data: top10
            });
        });
    } else if (scale === 'Year') {
        Order.aggregate([
            {
                $group: {
                    _id: {$year: '$date'},
                    count: {$sum: '$cart.totalQty'}
                }
            }]).exec(function (e, d) {
            res.render('admin/dashboard', {
                scale: scale,
                statData: d,
                top10Data: top10
            });
        });
    }
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function isAdmin(req, res, next) {
    if(req.isAuthenticated()){
        if(req.user.role === 'admin'){
            return next();
        }
    }
    res.redirect('/user/access-restrict');
}

module.exports = router;