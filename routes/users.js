var express = require('express');
var router = express.Router();
//var csrf = require('csurf');
var passport = require('passport');
var mailer = require('../utilities/mailer');

var Cart = require('../model/cart');
var Order = require('../model/order');
var Card = require('../model/card');
var User = require('../model/user');

//var csrfProtection = csrf();
//router.use(csrfProtection);

router.get('/profile', isLoggedIn, function (req, res, next) {
    var msg = req.flash('msg')[0];
    Order.find({user: req.user}, function (err, orders) {
        if(err){
            return res.write('Error!');
        }
        var cart;
        orders.forEach(function (order) {
            cart = new Cart(order.cart);
            order.items = cart.generateArray();
        });
        Card.findById(req.user.cardId, function (err, card) {
            if(err){
                return res.write('Error!');
            }
            res.render('user/profile', {orders: orders, card: card, msg: msg, noMessage:!msg});
        });
    });
});

router.get('/user_list', isLoggedIn, function (req, res, next) {
    User.find(function (err, users, res) {
        if (err) return res.sendStatus(500);
        res.render('admin/users', {userList: users});
    });
});

router.get('/logout', isLoggedIn, function (req, res, next) {
    req.logout();
    req.session.destroy(function (err) {
        res.redirect('/');
    });
});

router.get('/delete-card', isUser, function (req, res, next) {
    Card.remove({_id: req.user.cardId}, function (err, removed) {
        if(removed) {
            req.user.cardId = null;
            req.user.save(function (err, result) {
                req.flash('msg', 'Credit card information deleted');
                return res.redirect('/user/profile');
            })
        }
    });
});

router.get('/update-card', isLoggedIn, isUser, function (req, res, next) {
    Card.findById(req.user.cardId, function (err, card) {
        if(err){
            return res.write('Error!');
        }
        res.render('user/update-card', {card: card});
    });
});

router.post('/update-card', isLoggedIn, isUser, function (req, res, next) {
    var oldUrl = req.session.oldUrl;
    Card.findById(req.user.cardId, function (err, card) {
        if(err){
            return res.write('Error!');
        }
        if(card){
            card.holderName = req.body.holderName;
            card.number = req.body.number;
            card.expMonth = req.body.expMonth;
            card.expYear = req.body.expYear;
            card.cvc = req.body.cvc;
            card.save(function (err, result) {
                if(err){
                    req.flash('msg','Credit card failed to update');
                } else {
                    req.flash('msg','Credit card updated successfully');
                }
                if(oldUrl){
                    req.session.oldUrl = null;
                    res.redirect(oldUrl);
                } else {
                    res.redirect('/user/profile');
                }
            });
        } else {
            card = new Card({
                holderName: req.body.holderName,
                number: req.body.number,
                expMonth:  req.body.expMonth,
                expYear:  req.body.expYear,
                cvc: req.body.cvc
            });
            card.save(function (err, result) {
                if(err){
                    req.flash('msg','Credit card failed to update');
                    return res.redirect('/user/profile');
                }
                req.user.cardId = card;
                req.user.save(function (err, result) {
                    if(err){
                        req.flash('msg','Credit card failed to update');
                    } else {
                        req.flash('msg','Credit card updated successfully');
                    }
                    if(oldUrl){
                        req.session.oldUrl = null;
                        res.redirect(oldUrl);
                    } else {
                        res.redirect('/user/profile');
                    }
                });
            });
        }
    });
});

router.get('/update-profile', isLoggedIn, function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/update-profile', {messages: messages, hasErrors: messages.length > 0});
});

router.post('/update-profile', isLoggedIn, function (req, res, next) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    var errors = req.validationErrors();
    if(errors){
        var messages = [];
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        req.flash('error', messages);
        return res.redirect('/user/update-profile/');
    }
    req.user.email = req.body.email;
    req.user.firstName = req.body.firstName;
    req.user.lastName = req.body.lastName;
    req.user.dob = req.body.dob;
    req.user.phone = req.body.phone;
    req.user.address = req.body.address;
    req.user.imagePath = req.body.thumbnail;
    req.user.save(function (err, result) {
        if(err){
            req.flash('msg', 'User profile failed to update');
        } else {
            req.flash('msg', 'User profile updated successfully');
        }
        res.redirect('/user/profile');
    });
});

router.get('/update-password', isLoggedIn, function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/update-password', {messages: messages, hasMessage: messages.length > 0});
});

router.post('/update-password', isLoggedIn, function (req, res, next) {
    req.checkBody('password', 'Invalid password').notEmpty().isLength({min: 4});
    var errors = req.validationErrors();
    if(errors){
        var messages;
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        req.flash('error', messages);
        return res.redirect('/user/update-password/');
    }
    if( req.body.password !== req.body.repeatPassword){
        req.flash('error', "Password doesn't match");
        return res.redirect('/user/update-password/');
    }
    req.user.password = req.user.encryptPassword(req.body.password);
    req.user.save(function (err, result) {
        req.flash('msg', 'Password updated.');
        return res.redirect('/user/profile');
    });
});

router.get('/access-restrict', function (req, res, next) {
    res.render('user/access-restrict');
});

router.use('/', notLoggedIn, function (req, res, next) {
    next();
});

router.get('/signup', function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/signup', {/*csrfToken: req.csrfToken(),*/ messages: messages, hasErrors: messages.length > 0});
});

router.post('/signup', function (req, res, next) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('password', 'Invalid password').notEmpty().isLength({min: 4});
    var errors = req.validationErrors();
    var messages = [];
    if(errors) {
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
    }
    if(req.body.password !== req.body.repeatPassword){
        messages.push("Password doesn't match");
    }
    if(messages.length > 0){
        req.flash('error', messages);
        return res.redirect('/user/signup');
    }
    User.findOne({'email': req.body.email}, function (err, user) {
        if(err){
            console.log('Error');
        }
        if(user){
            req.flash('error', 'Email is already in use');
            return res.redirect('/user/signup');
        }
        var code = generateCode();
        var newUser = new User();
        newUser.email = req.body.email;
        newUser.password = newUser.encryptPassword(req.body.password);
        newUser.role = 'user';
        newUser.activated = false;
        newUser.activationCode = code;
        newUser.save(function (err, result) {
            console.log(result);
            if(err){
                console.log('Error');
            }
            var link = 'http://localhost:3000/user/activate/' + code;
            var text = 'Activate your account';
            mailer.send(newUser.email, link, text);
            req.flash('userMsg', 'Check your email for account activation instruction');
            res.redirect('/user/message');
        });
    });
});

router.get('/signin', function (req, res, next) {
    var messages = req.flash('error');
    var resetMsg = req.flash('userMsg')[0];
    res.render('user/signin', {/*csrfToken: req.csrfToken(),*/ messages: messages, resetMsg: resetMsg, hasErrors: messages.length > 0});
});

router.post('/signin', passport.authenticate('local.signin', {
    failureRedirect: '/user/signin',
    failureFlash: true
}), function (req, res, next) {
    if(req.session.oldUrl){
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});

router.get('/forgot-password', function (req, res, next) {
    var messages = req.flash('error');
    res.render('user/forgot-password', {messages: messages, hasMessage: messages.length > 0});
});

router.post('/forgot-password', function (req, res, next) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    var errors = req.validationErrors();
    var messages = [];
    if(errors){
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        req.flash('error', messages);
        return res.redirect('/user/forgot-password');
    }
    User.findOne({'email': req.body.email}, function (err, user) {
        if(!user) {
            messages.push('Email is not registered');
            req.flash('error', messages);
            return res.redirect('/user/forgot-password');
        }

        var code = generateCode();
        req.session.resetRequest = {
            code: code,
            user: user._id
        };
        console.log(req.session.resetRequest);

        var text = 'Reset your password';
        var link = 'http://localhost:3000/user/forgot-password/change/'+ code;

        mailer.send(user.email, link, text);
        req.flash('userMsg', 'Check your email for a link to reset password');
        res.redirect('/user/message');
    });
});

router.get('/forgot-password/change/:code', function (req, res, next) {
    var request = req.session.resetRequest;
    var messages = req.flash('error');
    if(request){
        if(request.code.toString() === req.params.code){
            return res.render('user/update-password', {isForgot: true, messages: messages, hasMessage: messages.length > 0});
        }
    }
    req.flash('userMsg','Problem fetching security code');
    res.redirect('/user/message');
});

router.post('/forgot-password/change', function (req, res, next) {
    req.checkBody('password', 'Invalid password').notEmpty().isLength({min: 4});
    var errors = req.validationErrors();
    if(errors){
        var messages;
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        req.flash('error', messages);
        return res.redirect('/user/forgot-password/change/'+ req.session.resetRequest.code);
    }
    if( req.body.password !== req.body.repeatPassword){
        req.flash('error', "Password doesn't match");
        return res.redirect('/user/forgot-password/change/'+ req.session.resetRequest.code);
    }
    User.findById(req.session.resetRequest.user, function (err, user) {
        user.password = user.encryptPassword(req.body.password);
        user.save(function (err, result) {
            req.session.resetRequest = null;
            req.flash('userMsg', 'Password reset completed. Sign in to your account to continue');
            return res.redirect('/user/signin');
        });
    });
});

router.get('/message', function (req, res, next) {
    var userMsg = req.flash('userMsg')[0];
    res.render('user/message', {userMsg: userMsg});
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function isAdmin(req, res, next) {
    if(req.isAuthenticated()){
        if(req.user.role === 'admin'){
            return next();
        }
    }
    res.redirect('/user/access-restrict');
}

function isUser(req, res, next) {
    if(req.isAuthenticated()){
        if(req.user.role === 'user'){
            return next();
        }
    }
    res.redirect('/user/access-restrict');
}

function notLoggedIn(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function generateCode() {
    var max = 999999;
    var min = 100000;
    return Math.floor(Math.random()*(max - min + 1)) + min;
}
