var passport = require('passport');
var User = require('../model/user');
var LocalStrategy = require('passport-local').Strategy;
var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');
var options = {
    viewEngine: {
        extname: '.hbs',
        layoutsDir: 'views/email/',
        defaultLayout : 'template'
    },
    viewPath: 'views/email/',
    extName: '.hbs'
};

passport.serializeUser(function (user,done) {
    done(null, user.id);
});

passport.deserializeUser(function (id,done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

/*passport.use('local.signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('password', 'Invalid password').notEmpty().isLength({min: 4});
    var errors = req.validationErrors();
    if(errors) {
        var messages = [];
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    if(password !== req.body.repeatPassword){
        return done(null, false, req.flash('error', "Password doesn't match"));
    }
    User.findOne({'email': email}, function (err, user) {
        if(err){
            return done(err);
        }
        if(user){
            return done(null, false, {message: 'Email is already in use.'});
        }
        var code = generateCode();
        var newUser = new User();
        newUser.email = email;
        newUser.password = newUser.encryptPassword(password);
        newUser.role = 'user';
        newUser.activated = false;
        newUser.activationCode = code;
        newUser.save(function (err, result) {
            console.log(result);
            if(err){
                return done(err);
            }
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'storegame786@gmail.com',
                    pass: 'storegame786storegame786'
                }
            });
            transporter.use('compile', hbs(options));
            var mailOptions = {
                from: 'storegame786@gmail.com',
                to: newUser.email,
                subject: 'Activate your account',
                template: 'email-body',
                context: {
                    link: 'http://localhost:3000/user/activate/' + code,
                    text: 'Activate your account'
                }
            };
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
            return done(null, false, 'Check your email for account activation instruction');
        });
    });
}));*/

passport.use('local.signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done){
    req.checkBody('email', 'Invalid email.').notEmpty().isEmail();
    req.checkBody('password', 'Invalid password.').notEmpty();
    var errors = req.validationErrors();
    if(errors){
        var messages = [];
        errors.forEach(function (error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({'email': email}, function (err, user) {
        if(err){
            return done(err);
        }
        if(!user){
            return done(null, false, {message: 'No user found.'});
        }
        if(!user.activated){
            return done(null, false, {message: 'Account is not activated. Check your email.'});
        }
        if(!user.validPassword(password)){
            return done(null, false, {message: 'Wrong password.'});
        }
        return done(null, user);
    });
}));

function generateCode() {
    var max = 999999;
    var min = 100000;
    return Math.floor(Math.random()*(max - min + 1)) + min;
}