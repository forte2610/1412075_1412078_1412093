var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    address: {type: String, required: true},
    name: {type: String, required: true},
    phone: {type: String, required: true},
    date: {type: Date, required: true},
    received: {type: Boolean, required: true},
    cart: {type: Object, required: true},
    paymentId: {type: String, required: true}
});

module.exports = mongoose.model('Order', schema);