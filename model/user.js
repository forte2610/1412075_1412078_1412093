var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var schema = new Schema({
    email: {type: String, required: true},
    password: {type: String, required: true},
    role: {type: String, required: true},
    imagePath: {type: String, required: false},
    firstName: {type: String, required: false},
    lastName: {type: String, required: false},
    address: {type: String, required: false},
    phone: {type: String, required: false},
    dob: {type: String, required: false},
    activated: {type: Boolean, required: true},
    activationCode: {type: String ,required: false},
    cardId: {type: Schema.Types.ObjectId, required: false, ref: 'Card'}
});

schema.methods.encryptPassword = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

schema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

schema.methods.getFullName = function () {
    if(lastName.length === 0){
        return firstName;
    } else {
        return firstName + " " + lastName;
    }
};

module.exports = mongoose.model('User', schema);