var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User', required: false},
    product: {type: Schema.Types.ObjectId, ref: 'Product', required: true},
    name: {type: String, required: true},
    content: {type: String, required: true},
    imagePath: {type: String, required: false},
    date: {type: String, required: true}
});

schema.methods.getDate = function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }
    today = mm + '/' + dd + '/' + yyyy;
    return today;
};

module.exports = mongoose.model('Comment', schema);