var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    holderName: {type: String, required: true},
    number: {type: String, required: true},
    expMonth: {type: String, required: true},
    expYear: {type: String, required: true},
    cvc: {type: String, required: true}
});

module.exports = mongoose.model('Card', schema);