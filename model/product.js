var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var schema = new Schema({
    imagePath: {type: String, required: true},
    title: {type: String, required: true},
    genre: {type: Object, required: true},
    quantity: {type: Number, required: true},
    platform: {type: String, required: true},
    description: {type: String, required: true},
    price: {type: Number, required: true}
});
//TODO: more data
module.exports = mongoose.model('Product',schema);