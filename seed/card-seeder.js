var mongoose = require('mongoose');
mongoose.connect('localhost:27017/shopping');
var Card = require('../model/card');

var cards = [
    new Card({
        holderName: 'Jenny Rosen',
        number: '4242 4242 4242 4242',
        expMonth: '12',
        expYear: '2018',
        cvc: '123'
    })
];

var done = 0;
for (var i = 0; i < cards.length; i++) {
    cards[i].save(function (err, result) {
            done++;
            if (done === cards.length) {
                exit();
            }
        }
    );
}

function exit() {
    mongoose.disconnect();
}
