var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
mongoose.connect('localhost:27017/shopping');
var User = require('../model/user');

var users = [
    new User({
        email: 'ledanofficial@gmail.com',
        password: bcrypt.hashSync('user', bcrypt.genSaltSync(5), null),
        role: 'user',
        activated: true,
        firstName: 'User',
        lastName: 'McUser',
        phone: '01234567890',
        dob: '11/11/1996',
        imagePath: 'http://7108-presscdn-0-78.pagely.netdna-cdn.com/wp-content/uploads/2013/09/person-to-person-business.jpg'
    }),
    new User({
        email: 'admin@admin.com',
        password: bcrypt.hashSync('admin', bcrypt.genSaltSync(5), null),
        role: 'admin',
        activated: true,
        firstName: 'Admin',
        lastName: 'McAdmin',
        phone: '09876543210',
        dob: '16/02/1996',
        imagePath: 'https://thetab.com/blogs.dir/109/files/2016/03/12115790-10205099232295943-3669427505802647685-n-11-2.jpg'
    })
];
var done = 0;
for (var i = 0; i < users.length; i++) {
    users[i].save(function (err, result) {
            done++;
            if (done === users.length) {
                exit();
            }
        }
    );
}
function exit() {
    mongoose.disconnect();
}

